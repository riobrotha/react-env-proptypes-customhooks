import axios from "axios";
import { useCallback, useEffect, useState } from "react";

const useAsync = (endpoint) => {
  const [posts, setPost] = useState([]);
  const [error, setError] = useState("");

  const fetchPost = useCallback(() => {
    const fetching = async () => {
      try {
        const { data } = await axios.get(endpoint);
        setPost(data);
      } catch (err) {
        setError(err);
      }
    };

    fetching();
  }, [endpoint]);

  useEffect(() => {
    fetchPost();
  }, [fetchPost]);

  return { posts, error };
};

export default useAsync;
