import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Typography,
} from "@mui/material";
import { red } from "@mui/material/colors";
import React from "react";
import PropTypes from "prop-types";

function MyCard({ data }) {
  return (
    <Card sx={{ maxWidth: "100%" }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            {data.author[0]}
          </Avatar>
        }
        title={data.title}
        subheader={data.datePost}
      />
      <CardMedia
        component="img"
        height="194"
        image={data.img}
        alt="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {data.description}
        </Typography>
      </CardContent>
    </Card>
  );
}

MyCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default MyCard;
