import { Avatar, Box, Grid, Typography } from "@mui/material";
import MyCard from "./MyCard";
import { grey, red } from "@mui/material/colors";
import useAsync from "../Hooks/useAsync";

export default function Content() {
  //const [posts, setPost] = useState([]);

  const { posts } = useAsync("http://localhost:3004/postgenerated");

  return (
    <Grid container spacing={2} sx={{ marginBottom: "16px" }}>
      <Grid container item spacing={2} xs={12} md={8}>
        {posts?.map((item) => {
          return (
            <Grid key={item.post_id} item xs={12} md={6}>
              <MyCard data={item} />
            </Grid>
          );
        })}
      </Grid>
      <Grid item xs={12} md={4}>
        <Box sx={{ display: "flex", flexDirection: "column", gap: 1.5 }}>
          {posts?.map((item) => {
            return (
              <Box
                key={item.post_id}
                sx={{
                  display: "flex",
                  gap: "20px",
                  paddingX: { xs: "10px", md: "20px" },
                  paddingY: "10px",
                  alignItems: "center",
                }}
              >
                <Avatar
                  sx={{ bgcolor: grey[500], width: "64px", height: "64px" }}
                  src={item.img}
                ></Avatar>
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Typography
                    variant="p"
                    sx={{ color: red[400], fontSize: "22px" }}
                  >
                    {item.author}
                  </Typography>
                  <Typography
                    variant="p"
                    sx={{ color: grey[600], fontSize: "16px" }}
                  >
                    {item.datePost}
                  </Typography>
                </Box>
              </Box>
            );
          })}
        </Box>
      </Grid>
    </Grid>
  );
}
