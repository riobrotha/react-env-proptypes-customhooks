import Container from "@mui/material/Container";
import Navbar from "./Components/Navbar";
import Content from "./Components/Content";
import { createTheme, ThemeProvider } from "@mui/material";

const theme = createTheme({
  palette: {
    primary: {
      main: process.env.REACT_APP_COLOR_PRIMARY,
    },
  },
  typography: {
    fontFamily: "Poppins, sans-serif",
  },
});

function App() {
  return (
    <>
      <ThemeProvider theme={theme}>
        <Navbar />
        <Container maxWidth="lg" sx={{ mt: "32px" }}>
          <Content />
        </Container>
      </ThemeProvider>
    </>
  );
}

export default App;
